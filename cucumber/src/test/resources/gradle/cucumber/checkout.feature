Feature: Checkout

  # first iteration
  Scenario: Checkout a banana
    Given the price of a "banana" is 40c
    When I checkout 1 "banana"
    Then the total price should be 40c

  # 8th iteration
  Scenario Outline: Checkout bananas
    Given the price of a "banana" is 40c
    When I checkout <count> "banana"
    Then the total price should be <total>c

    Examples:
    | count | total |
    | 1     | 40    |
    | 2     | 80    |

  # 9th iteration
  Scenario: Two bananas scanned separately
    Given the price of a "banana" is 40c
    When I checkout 1 "banana"
    And I checkout 1 "banana"
    Then the total price should be 80c

  # 10th iteration
  Scenario: A banana and an apple
    Given the price of a "banana" is 40c
    And the price of a "apple" is 25c
    And the price of a "cucumber" is 10c
    When I checkout 1 "banana"
    And I checkout 1 "apple"
    And I checkout 1 "cucumber"
    Then the total price should be 75c
